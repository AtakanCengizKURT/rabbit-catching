//
//  ViewController.swift
//  Rabbit Catching
//
//  Created by Atakan Cengiz KURT on 18.09.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var highScoreLabel: UILabel!
    @IBOutlet weak var rabbit1: UIImageView!
    @IBOutlet weak var rabbit2: UIImageView!
    @IBOutlet weak var rabbit3: UIImageView!
    @IBOutlet weak var rabbit4: UIImageView!
    @IBOutlet weak var rabbit5: UIImageView!
    @IBOutlet weak var rabbit6: UIImageView!
    @IBOutlet weak var rabbit7: UIImageView!
    @IBOutlet weak var rabbit8: UIImageView!
    @IBOutlet weak var rabbit9: UIImageView!

    
    var score = 0
    var timer = Timer()
    var counter = 0
    var rabbitArray = [UIImageView]()
    var hideTimer = Timer()
    var level2 = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let highScore = UserDefaults.standard.object(forKey: "highscore")
        
        if highScore == nil {
            highScoreLabel.text = "0"
        }
        if let newScore = highScore as? Int {
            highScoreLabel.text = String(newScore)
        }
        
        scoreLabel.text = "Score : \(score)"
        rabbit1.isUserInteractionEnabled = true
        rabbit2.isUserInteractionEnabled = true
        rabbit3.isUserInteractionEnabled = true
        rabbit4.isUserInteractionEnabled = true
        rabbit5.isUserInteractionEnabled = true
        rabbit6.isUserInteractionEnabled = true
        rabbit7.isUserInteractionEnabled = true
        rabbit8.isUserInteractionEnabled = true
        rabbit9.isUserInteractionEnabled = true
        
        let recognizer1 = UITapGestureRecognizer(target: self, action: #selector(ViewController.increaseScore))
        let recognizer2 = UITapGestureRecognizer(target: self, action: #selector(ViewController.increaseScore))
        let recognizer3 = UITapGestureRecognizer(target: self, action: #selector(ViewController.increaseScore))
        let recognizer4 = UITapGestureRecognizer(target: self, action: #selector(ViewController.increaseScore))
        let recognizer5 = UITapGestureRecognizer(target: self, action: #selector(ViewController.increaseScore))
        let recognizer6 = UITapGestureRecognizer(target: self, action: #selector(ViewController.increaseScore))
        let recognizer7 = UITapGestureRecognizer(target: self, action: #selector(ViewController.increaseScore))
        let recognizer8 = UITapGestureRecognizer(target: self, action: #selector(ViewController.increaseScore))
        let recognizer9 = UITapGestureRecognizer(target: self, action: #selector(ViewController.increaseScore))
        
        
        
        rabbit1.addGestureRecognizer(recognizer1)
        rabbit2.addGestureRecognizer(recognizer2)
        rabbit3.addGestureRecognizer(recognizer3)
        rabbit4.addGestureRecognizer(recognizer4)
        rabbit5.addGestureRecognizer(recognizer5)
        rabbit6.addGestureRecognizer(recognizer6)
        rabbit7.addGestureRecognizer(recognizer7)
        rabbit8.addGestureRecognizer(recognizer8)
        rabbit9.addGestureRecognizer(recognizer9)
        
        
        //Timer
        counter = 30
        timeLabel.text = "\(counter)"
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.countDown), userInfo: nil, repeats: true)
        
        hideTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.hideRabbit), userInfo: nil, repeats: true)
        
        //Array
        rabbitArray.append(rabbit1)
        rabbitArray.append(rabbit2)
        rabbitArray.append(rabbit3)
        rabbitArray.append(rabbit4)
        rabbitArray.append(rabbit5)
        rabbitArray.append(rabbit6)
        rabbitArray.append(rabbit7)
        rabbitArray.append(rabbit8)
        rabbitArray.append(rabbit9)
        
        
    }
    
    @objc func hideRabbit(){
        for rabbit in rabbitArray{
            rabbit.isHidden = true
        }
        let random = Int(arc4random_uniform(UInt32(rabbitArray.count - 1)))
        rabbitArray[random].isHidden = false
    }
    
    
    @objc func countDown(){
        counter = counter - 1
        timeLabel.text = "\(counter)"
        if counter == 0{
            timer.invalidate()
            hideTimer.invalidate()
            
            
            
            for rabbit in rabbitArray{
                rabbit.isHidden = true
            }
            
            
            let alert = UIAlertController(title: "Time", message: "Time's Up", preferredStyle: UIAlertControllerStyle.alert)
            let okButton = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
            let replayButton = UIAlertAction(title: "Replay", style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
                self.score = 0
                self.scoreLabel.text = "Score : \(self.score)"
                self.counter = 30
                self.timeLabel.text = "\(self.counter)"
                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.countDown), userInfo: nil, repeats: true)
                self.hideTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.hideRabbit), userInfo: nil, repeats: true)
                self.level2 = true
                
            })
            
            alert.addAction(okButton)
            alert.addAction(replayButton)
            self.present(alert, animated: true, completion: nil)
        }
        //En Yüksek skoru sakla
        if self.score > Int(highScoreLabel.text!)! {
            UserDefaults.standard.set(self.score, forKey: "highscore")
            highScoreLabel.text = "High Score: \(String(self.score))"
        }
        
    }
    @objc func increaseScore(){
        
        score = score + 1
        scoreLabel.text = "Score : \(score)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func level(timeInterval: Double, counter: Int){
        let alert = UIAlertController(title: "Time", message: "Time's Up", preferredStyle: UIAlertControllerStyle.alert)
        let okButton = UIAlertAction(title: "Finish Game", style: UIAlertActionStyle.default, handler: nil)
        let replayButton = UIAlertAction(title: "Go on", style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
            self.scoreLabel.text = "Score : \(self.score)"
            self.counter = counter
            self.timeLabel.text = "\(self.counter)"
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.countDown), userInfo: nil, repeats: true)
            self.hideTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(ViewController.hideRabbit), userInfo: nil, repeats: true)
            
        })
        
        alert.addAction(okButton)
        alert.addAction(replayButton)
        self.present(alert, animated: true, completion: nil)
    }

}

